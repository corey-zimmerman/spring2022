<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Corey's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
</head>
<body>
    <header>
        <h1>Corey's Homepage</h1>
    </header>

    <nav>
        <ul>
            <li><a href="/">Homepage</a></li>
            <li><a href="#">Loop Demo</a></li>
            <li><a href="#">Countdown</a></li>

        </ul>
    </nav>

    <main>
        <img src="img/giannis.jpg" alt="Corey Image"/>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam faucibus pellentesque libero, id ultrices velit finibus pretium. Vestibulum auctor massa quis lacus efficitur lacinia. Donec eu venenatis mi, eget placerat enim. Duis sapien odio, semper a sollicitudin quis, tempor vel dolor. Maecenas lobortis dolor a quam ornare maximus vitae eget ante. Integer bibendum in ligula eu ullamcorper. Cras facilisis lorem libero, ut tincidunt turpis convallis a. In porta tempor ultricies. In vulputate consequat nisl, vel porta lorem interdum at. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur ultricies dui at ante ornare luctus. Aliquam vitae imperdiet est, vel egestas odio. Fusce maximus bibendum ligula in rhoncus.</p>

    </main>

    <footer>
        &copy; <?= date("Y") ?> Corey Zimmerman
    </footer>
</body>
</html>